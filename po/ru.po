# Russian translation for camera-app
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the camera-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: camera-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-05-17 10:34+0000\n"
"PO-Revision-Date: 2023-01-16 02:48+0000\n"
"Last-Translator: Sergii Horichenko <m@sgg.im>\n"
"Language-Team: Russian <https://hosted.weblate.org/projects/lomiri/lomiri-"
"camera-app/ru/>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 4.15.1-dev\n"
"X-Launchpad-Export-Date: 2016-12-01 04:53+0000\n"

#: lomiri-camera-app.desktop.in:4 lomiri-camera-app.desktop.in:6
#: Information.qml:106
msgid "Camera"
msgstr "Камера"

#: lomiri-camera-app.desktop.in:5
msgid "Camera application"
msgstr "Фотокамера"

#: lomiri-camera-app.desktop.in:7
msgid "Photos;Videos;Capture;Shoot;Snapshot;Record"
msgstr "Фото;Видео;Захват;Съёмка;Снимок;Запись"

#: lomiri-camera-app.desktop.in:10
msgid "lomiri-camera-app"
msgstr ""

#: lomiri-barcode-reader-app.desktop.in:4
#: lomiri-barcode-reader-app.desktop.in:6
msgid "Barcode Reader"
msgstr ""

#: lomiri-barcode-reader-app.desktop.in:5
#, fuzzy
#| msgid "Camera application"
msgid "Barcode Reader application"
msgstr "Фотокамера"

#: lomiri-barcode-reader-app.desktop.in:7
msgid "QR;Code;Reader"
msgstr ""

#: lomiri-barcode-reader-app.desktop.in:10
msgid "lomiri-barcode-reader-app"
msgstr ""

#: AdvancedOptions.qml:23 PhotogridView.qml:69 SlideshowView.qml:77
msgid "Settings"
msgstr "Настройки"

#: AdvancedOptions.qml:27
msgid "Close"
msgstr "Закрыть"

#: AdvancedOptions.qml:35 Information.qml:20 PhotogridView.qml:78
#: SlideshowView.qml:86
msgid "About"
msgstr "О программе"

#: AdvancedOptions.qml:66
msgid "Add date stamp on captured images"
msgstr "Добавлять дату и время на отснятые изображения"

#. TRANSLATORS: this refers to the opacity  of date stamp added to captured images
#: AdvancedOptions.qml:89
msgid "Format"
msgstr "Форматировать"

#: AdvancedOptions.qml:142
msgid "Date formatting keywords"
msgstr "Ключевые слова форматирования даты"

#: AdvancedOptions.qml:147
msgid "the day as number without a leading zero (1 to 31)"
msgstr ""
"1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, "
"22, 23, 24, 25, 26, 27, 28, 29, 30, 31"

#: AdvancedOptions.qml:148
msgid "the day as number with a leading zero (01 to 31)"
msgstr ""
"01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, "
"20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31"

#: AdvancedOptions.qml:149
msgid "the abbreviated localized day name (e.g. 'Mon' to 'Sun')."
msgstr "Пн, Вт, Ср, Чт, Пт, Сб, Вс."

#: AdvancedOptions.qml:150
msgid "the long localized day name (e.g. 'Monday' to 'Sunday')."
msgstr "Понедельник, Вторник, Среда, Четверг, Пятница, Суббота, Воскресенье."

#: AdvancedOptions.qml:151
msgid "the month as number without a leading zero (1 to 12)"
msgstr "1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12"

#: AdvancedOptions.qml:152
msgid "the month as number with a leading zero (01 to 12)"
msgstr "01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12"

#: AdvancedOptions.qml:153
msgid "the abbreviated localized month name (e.g. 'Jan' to 'Dec')."
msgstr "сокращенное название месяца (например, от «Янв» до «Дек»)."

#: AdvancedOptions.qml:154
msgid "the long localized month name (e.g. 'January' to 'December')."
msgstr "полное название месяца (например, от «Январь» до «Декабрь»)."

#: AdvancedOptions.qml:155
msgid "the year as two digit number (00 to 99)"
msgstr "год в виде двухзначного числа (от 00 до 99)"

#: AdvancedOptions.qml:156
msgid ""
"the year as four digit number. If the year is negative, a minus sign is "
"prepended in addition."
msgstr ""
"год в виде четырехзначного числа. Если год отрицательный, дополнительно "
"добавляется знак минус."

#: AdvancedOptions.qml:157
msgid "the hour without a leading zero (0 to 23 or 1 to 12 if AM/PM display)"
msgstr ""
"час без начального нуля (от 0 до 23 или от 1 до 12 при отображении ДП/ПП)"

#: AdvancedOptions.qml:158
msgid "the hour with a leading zero (00 to 23 or 01 to 12 if AM/PM display)"
msgstr ""
"час с начальным нулем (от 00 до 23 или от 01 до 12, если отображается ДП/ПП)"

#: AdvancedOptions.qml:159
msgid "the hour without a leading zero (0 to 23, even with AM/PM display)"
msgstr "час без начального нуля (от 0 до 23, даже с отображением ДП/ПП)"

#: AdvancedOptions.qml:160
msgid "the hour with a leading zero (00 to 23, even with AM/PM display)"
msgstr "час с начальным нулем (от 00 до 23, даже с отображением ДП/ПП)"

#: AdvancedOptions.qml:161
msgid "the minute without a leading zero (0 to 59)"
msgstr "минуты без начального нуля (от 0 до 59)"

#: AdvancedOptions.qml:162
msgid "the minute with a leading zero (00 to 59)"
msgstr "минуты с начальным нулем (от 00 до 59)"

#: AdvancedOptions.qml:163
msgid "the second without a leading zero (0 to 59)"
msgstr "секунды без начального нуля (от 0 до 59)"

#: AdvancedOptions.qml:164
msgid "the second with a leading zero (00 to 59)"
msgstr "секунды с начальным нулем (от 00 до 59)"

#: AdvancedOptions.qml:165
msgid "the milliseconds without leading zeroes (0 to 999)"
msgstr "миллисекунды без начальных нулей (от 0 до 999)"

#: AdvancedOptions.qml:166
msgid "the milliseconds with leading zeroes (000 to 999)"
msgstr "миллисекунды с начальными нулями (от 000 до 999)"

#: AdvancedOptions.qml:167
msgid "use AM/PM display. AP will be replaced by either 'AM' or 'PM'."
msgstr "отображать ДП/ПП. AP будет заменен либо «ДП», либо «ПП»."

#: AdvancedOptions.qml:168
msgid "use am/pm display. ap will be replaced by either 'am' or 'pm'."
msgstr "отображать дп/пп. ap будет заменен либо «дп», либо «пп»."

#: AdvancedOptions.qml:169
msgid "the timezone (for example 'CEST')"
msgstr "часовой пояс (например 'CEST')"

#: AdvancedOptions.qml:178
msgid "Add to Format"
msgstr "Добавить в формат"

#. TRANSLATORS: this refers to the color of date stamp added to captured images
#: AdvancedOptions.qml:209
msgid "Color"
msgstr "Цвет"

#. TRANSLATORS: this refers to the alignment of date stamp within captured images (bottom left, top right,etc..)
#: AdvancedOptions.qml:278
msgid "Alignment"
msgstr "Положение"

#. TRANSLATORS: this refers to the opacity  of date stamp added to captured images
#: AdvancedOptions.qml:323
msgid "Opacity"
msgstr "Прозрачность"

#: AdvancedOptions.qml:346
msgid "Blurred Overlay"
msgstr "Размытое наложение"

#: AdvancedOptions.qml:367
msgid "Only Blur Preview overlay"
msgstr "Только предварительное наложение размытия"

#: DeleteDialog.qml:24
msgid "Delete media?"
msgstr "Удалить содержимое?"

#: DeleteDialog.qml:34 PhotogridView.qml:56 SlideshowView.qml:69
msgid "Delete"
msgstr "Удалить"

#: DeleteDialog.qml:40 ViewFinderOverlay.qml:1203 ViewFinderOverlay.qml:1217
#: ViewFinderOverlay.qml:1265 ViewFinderView.qml:409
msgid "Cancel"
msgstr "Отмена"

#: GalleryView.qml:270
msgid "No media available."
msgstr "Содержимого нет."

#: GalleryView.qml:305
msgid "Scanning for content..."
msgstr "Поиск содержимого..."

#: GalleryViewHeader.qml:84 SlideshowView.qml:46
msgid "Select"
msgstr "Выбрать"

#: GalleryViewHeader.qml:85
msgid "Edit Photo"
msgstr "Редактировать фото"

#: GalleryViewHeader.qml:85
msgid "Photo Roll"
msgstr "Фотопленка"

#: Information.qml:25
msgid "Back"
msgstr "Назад"

#: Information.qml:76
msgid "Get the source"
msgstr "Получить исходный код"

#: Information.qml:77
msgid "Report issues"
msgstr "Сообщить о проблемах"

#: Information.qml:78
msgid "Help translate"
msgstr "Помочь с переводом"

#: MediaInfoPopover.qml:14
#, qt-format
msgid "Width : %1"
msgstr "Ширина : %1"

#: MediaInfoPopover.qml:15
#, qt-format
msgid "Height : %1"
msgstr "Высота : %1"

#: MediaInfoPopover.qml:16
#, qt-format
msgid "Date : %1"
msgstr "Дата : %1"

#: MediaInfoPopover.qml:17
#, qt-format
msgid "Camera Model : %1"
msgstr "Модель камеры : %1"

#: MediaInfoPopover.qml:18
#, qt-format
msgid "Copyright : %1"
msgstr "Авторские права : %1"

#: MediaInfoPopover.qml:19
#, qt-format
msgid "Exposure Time : %1"
msgstr "Выдержка : %1"

#: MediaInfoPopover.qml:20
#, qt-format
msgid "F. Number : %1"
msgstr "Диафрагма : %1"

#: MediaInfoPopover.qml:21
#, qt-format
msgid "Sub-File type : %1"
msgstr "Тип данных : %1"

#: MediaInfoPopover.qml:43
msgid "Media Information"
msgstr "Данные о файле"

#: MediaInfoPopover.qml:48
#, qt-format
msgid "Name : %1"
msgstr "Название : %1"

#: MediaInfoPopover.qml:51
#, qt-format
msgid "Type : %1"
msgstr "Тип : %1"

#: MediaInfoPopover.qml:83
#, qt-format
msgid "With Flash : %1"
msgstr "Со вспышкой : %1"

#: MediaInfoPopover.qml:83
msgid "Yes"
msgstr "Да"

#: MediaInfoPopover.qml:83
msgid "No"
msgstr "Нет"

#: NoSpaceHint.qml:33
msgid "No space left on device, free up space to continue."
msgstr "Не хватает свободного места; освободите место, чтобы продолжить."

#: PhotoRollHint.qml:69
msgid "Swipe left for photo roll"
msgstr "Проведите влево для перехода к фотоленте"

#: PhotogridView.qml:42 SlideshowView.qml:54
msgid "Share"
msgstr "Поделиться"

#: SlideshowView.qml:62
msgid "Image Info"
msgstr "Информация об изображении"

#: SlideshowView.qml:98
msgid "Edit"
msgstr "Редактировать"

#: SlideshowView.qml:477
msgid "Back to Photo roll"
msgstr "Вернуться к фото"

#: UnableShareDialog.qml:25
msgid "Unable to share"
msgstr "Не удалось поделиться"

#: UnableShareDialog.qml:26
msgid "Unable to share photos and videos at the same time"
msgstr "Нельзя делиться фото и видео одновременно"

#: UnableShareDialog.qml:30
msgid "Ok"
msgstr "ОК"

#: ViewFinderOverlay.qml:469 ViewFinderOverlay.qml:492
#: ViewFinderOverlay.qml:520 ViewFinderOverlay.qml:543
#: ViewFinderOverlay.qml:628 ViewFinderOverlay.qml:695
msgid "On"
msgstr "Включено"

#: ViewFinderOverlay.qml:474 ViewFinderOverlay.qml:502
#: ViewFinderOverlay.qml:525 ViewFinderOverlay.qml:548
#: ViewFinderOverlay.qml:567 ViewFinderOverlay.qml:633
#: ViewFinderOverlay.qml:705
msgid "Off"
msgstr "Не в сети"

#: ViewFinderOverlay.qml:497
msgid "Auto"
msgstr "Авто"

#: ViewFinderOverlay.qml:534
msgid "HDR"
msgstr "HDR"

#: ViewFinderOverlay.qml:572
msgid "5 seconds"
msgstr "5 секунд"

#: ViewFinderOverlay.qml:577
msgid "15 seconds"
msgstr "15 секунд"

#: ViewFinderOverlay.qml:595
msgid "Fine Quality"
msgstr "Высокое качество"

#: ViewFinderOverlay.qml:600
msgid "High Quality"
msgstr "Высокое качество"

#: ViewFinderOverlay.qml:605
msgid "Normal Quality"
msgstr "Обычное качество"

#: ViewFinderOverlay.qml:610
msgid "Basic Quality"
msgstr "Начальное качество"

#. TRANSLATORS: this will be displayed on an small button so for it to fit it should be less then 3 characters long.
#: ViewFinderOverlay.qml:643
msgid "SD"
msgstr "SD"

#: ViewFinderOverlay.qml:651
msgid "Save to SD Card"
msgstr "Сохранить на SD-карту"

#: ViewFinderOverlay.qml:656
msgid "Save internally"
msgstr "Сохранить локально"

#: ViewFinderOverlay.qml:700
msgid "Vibrate"
msgstr "Вибрировать"

#: ViewFinderOverlay.qml:1200
msgid "Low storage space"
msgstr "Мало свободного места"

#: ViewFinderOverlay.qml:1201
msgid ""
"You are running out of storage space. To continue without interruptions, "
"free up storage space now."
msgstr ""
"Заканчивается свободное место для хранения. Чтобы избежать сбоев в работе, "
"освободите место сейчас."

#: ViewFinderOverlay.qml:1214
msgid "External storage not writeable"
msgstr "Запись на внешний носитель отключена"

#: ViewFinderOverlay.qml:1215
msgid ""
"It does not seem possible to write to your external storage media. Trying to "
"eject and insert it again might solve the issue, or you might need to format "
"it."
msgstr ""
"Не удаётся произвести запись на внешний носитель. Попробуйте извлечь и "
"заново вставить его. Возможно необходимо форматирование носителя."

#: ViewFinderOverlay.qml:1253
msgid "Cannot access camera"
msgstr "Нет доступа к камере"

#: ViewFinderOverlay.qml:1254
msgid ""
"Camera app doesn't have permission to access the camera hardware or another "
"error occurred.\n"
"\n"
"If granting permission does not resolve this problem, reboot your device."
msgstr ""
"Приложение камеры не имеет доступа к оборудованию камеры или возникла другая "
"ошибка.\n"
"\n"
"Если разрешение не устраняет эту проблему, перезагрузите телефон."

#: ViewFinderOverlay.qml:1256
msgid "Edit Permissions"
msgstr "Изменить разрешения"

#: ViewFinderView.qml:400
msgid "Capture failed"
msgstr "Не удалось сделать снимок"

#: ViewFinderView.qml:405
msgid ""
"Replacing your external media, formatting it, or restarting the device might "
"fix the problem."
msgstr ""
"Возможно проблему удастся исправить заменой внешнего носителя, его "
"форматированием или перезагрузкой устройства."

#: ViewFinderView.qml:406
msgid "Restarting your device might fix the problem."
msgstr "Перезагрузка устройства может решить проблему."

#: camera-app.qml:61
msgid "Flash"
msgstr "Вспышка"

#: camera-app.qml:62
msgid "Light;Dark"
msgstr "Светло;Темно"

#: camera-app.qml:65
msgid "Flip Camera"
msgstr "Перевернуть камеру"

#: camera-app.qml:66
msgid "Front Facing;Back Facing"
msgstr "Передняя камера;Задняя камера"

#: camera-app.qml:69
msgid "Shutter"
msgstr "Затвор"

#: camera-app.qml:70
msgid "Take a Photo;Snap;Record"
msgstr "Сделать фото;Мометальный снимок;Записать"

#: camera-app.qml:73
msgid "Mode"
msgstr "Режим"

#: camera-app.qml:74
msgid "Stills;Video"
msgstr "Фото;Видео"

#: camera-app.qml:78
msgid "White Balance"
msgstr "Баланс белого"

#: camera-app.qml:79
msgid "Lighting Condition;Day;Cloudy;Inside"
msgstr "Условия освещения;Ясно;Пасмурно;Помещение"

#: camera-app.qml:386
#, qt-format
msgid "<b>%1</b> photos taken today"
msgstr "<b>%1</b> фотографий сегодня"

#: camera-app.qml:387
msgid "No photos taken today"
msgstr "Сегодня нет фотографий"

#: camera-app.qml:397
#, qt-format
msgid "<b>%1</b> videos recorded today"
msgstr "<b>%1</b> видео сегодня"

#: camera-app.qml:398
msgid "No videos recorded today"
msgstr "Сегодня нет видео"

#~ msgid "/usr/share/lomiri-camera-app/lomiri-camera-app.png"
#~ msgstr "/usr/share/lomiri-camera-app/lomiri-camera-app.png"

#~ msgid "Gallery"
#~ msgstr "Галерея"

#~ msgid "Version %1"
#~ msgstr "Версия %1"

#~ msgid "Advanced Options"
#~ msgstr "Дополнительные настройки"
